package com.ndubkov.gpio;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiBcmPin;
import com.pi4j.io.gpio.RaspiGpioProvider;
import com.pi4j.io.gpio.RaspiPin;

/**
 * Created by Nickolai on 03.10.2016.
 */

public class GpioWorker {

    GpioController gpioController;

    public GpioWorker() {
        gpioController = GpioFactory.getInstance();
    }

    public PinState getState(Pin pin) {
        GpioPinDigitalOutput gpio = gpioController.provisionDigitalOutputPin(pin);
        gpio.setShutdownOptions(true, PinState.HIGH);
        PinState pinState = gpio.getState();
        gpioController.unprovisionPin(gpio);
        return pinState;
    }

    public PinState turnOn(Pin pin) {
        GpioPinDigitalOutput gpio = gpioController.provisionDigitalOutputPin(pin);
        gpio.setShutdownOptions(true, PinState.HIGH);
        gpio.low();
        PinState pinState = gpio.getState();
        gpioController.unprovisionPin(gpio);
        return pinState;
    }

    public PinState turnOff(Pin pin) {
        GpioPinDigitalOutput gpio = gpioController.provisionDigitalOutputPin(pin);
        gpio.setShutdownOptions(true, PinState.HIGH);
        gpio.high();
        PinState pinState = gpio.getState();
        gpioController.unprovisionPin(gpio);
        return pinState;
    }

    public PinState toggle(Pin pin) {
        GpioPinDigitalOutput gpio = gpioController.provisionDigitalOutputPin(pin);
        gpio.setShutdownOptions(true, PinState.HIGH);
        gpio.toggle();
        PinState pinState = gpio.getState();
        gpioController.unprovisionPin(gpio);
        return pinState;
    }
}
