package com.ndubkov.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ndubkov.model.device.Device;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by Nickolay on 21.11.2016.
 */

public class ConfigWorker {
    public static Observable<Device> getAvailableDevices() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("config/available_devices.json"));
            Type type = new TypeToken<List<Device>>() {
            }.getType();
            return Observable.from((Iterable<? extends Device>) new GsonBuilder()
                    .excludeFieldsWithoutExposeAnnotation()
                    .create()
                    .fromJson(br, type));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return Observable.error(e);
        }
    }
}
