package com.ndubkov;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ndubkov.log.Logger;
import com.ndubkov.model.device.DeviceInfo;
import com.ndubkov.model.Response;
import com.ndubkov.model.command.Command;
import com.ndubkov.model.Request;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rx.Observable;
import rx.schedulers.Schedulers;

public class Main {

    public static void main(String[] args) throws UnknownHostException {
        runServer().doOnError(Throwable::printStackTrace).subscribe();
    }


    private static boolean isStopped = false;

    private static Observable<Void> runServer() {
        return Observable.create(subscriber -> {
            try {
                ServerSocket serverSocket = new ServerSocket(4444);
                System.out.println("Smart home server started");
                while (!isStopped)
                    processClient(serverSocket.accept())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(Schedulers.io())
                            .doOnCompleted(() -> System.out.println("Client closed connection"))
                            .subscribe();
                serverSocket.close();
            } catch (IOException e) {
                subscriber.onError(e);
            }
            subscriber.onCompleted();
        });
    }

    static List<Socket> connectedClients = new ArrayList<>();

    private static Observable<Void> processClient(Socket clientSocket) {
        return Observable.create(subscriber -> {
            try {
                BufferedReader in = new BufferedReader(new
                        InputStreamReader(clientSocket.getInputStream()));
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                String message;
                System.out.println("Client connected");
                connectedClients.add(clientSocket);
                while ((message = in.readLine()) != null) {
                    onMessageReceived(message)
                            .subscribe(responseObject -> {
                                out.println(new Gson()
                                        .toJson(new Response<>(Response.ResponseStatus.SUCCESS, responseObject)));
                            }, throwable -> {
                                Response response = new Response(Response.ResponseStatus.FAILED,
                                        throwable.getMessage());
                                out.println(new Gson().toJson(response));
                            });
                }
                out.close();
                in.close();
                clientSocket.close();
                connectedClients.remove(clientSocket);
            } catch (IOException e) {
                subscriber.onError(e);
            }
            subscriber.onCompleted();
        });
    }

    private static Observable<?> onMessageReceived(String message) {
        Type type = new TypeToken<Request<Command>>() {
        }.getType();
        Request request = new GsonBuilder()
                .registerTypeAdapter(type, new Request.RequestDeserializer())
                .create()
                .fromJson(message, type);
        return request.getCommand().doCommand();
    }

    public static void deviceStateChanged(DeviceInfo deviceInfo) {
        /*for (Socket socket : connectedClients) {
            PrintWriter out = null;
            try {
                out = new PrintWriter(socket.getOutputStream(), true);
                out.println(new Gson()
                        .toJson(new Response<>(Response.ResponseStatus.SUCCESS, deviceInfo)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }
}
