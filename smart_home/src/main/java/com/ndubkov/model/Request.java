package com.ndubkov.model;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.ndubkov.model.command.Command;

import java.lang.reflect.Type;

/**
 * Created by Nickolay on 18.11.2016.
 */

public class Request<T extends Command> {

    String commandType;
    T command;

    public Request(String commandType, T command) {
        this.commandType = commandType;
        this.command = command;
    }

    public static class RequestDeserializer implements JsonDeserializer<Request<Command>> {

        @Override
        public Request<Command> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            String commandType = json.getAsJsonObject().getAsJsonPrimitive("commandType").getAsString();
            try {
                Type commandClass = Class.forName("com.ndubkov.model.command." + commandType);
                JsonElement commandJson = json.getAsJsonObject().get("command");
                Command command = context.deserialize(commandJson, commandClass);
                return new Request<Command>(commandType, command);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public T getCommand() {
        return command;
    }
}
