package com.ndubkov.model.device;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nickolay on 17.11.2016.
 */

public class Device {
    @Expose
    String deviceId;
    @Expose(serialize = false)
    String gpioPinName;
    @Expose
    String deviceTitle;
    @Expose
    String deviceCategory;

    public Device(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getGpioPinName() {
        return gpioPinName;
    }
}
