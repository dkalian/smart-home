package com.ndubkov.model.device;

import com.pi4j.io.gpio.PinState;

/**
 * Created by Nickolay on 17.11.2016.
 */

public class DeviceInfo {
    Device device;
    DeviceState deviceState;

    public DeviceInfo(String deviceId, DeviceState deviceState) {
        this.device = new Device(deviceId);
        this.deviceState = deviceState;
    }

    public DeviceInfo(Device device, DeviceState deviceState) {
        this.device = device;
        this.deviceState = deviceState;
    }

    public enum DeviceState {
        ON, OFF;

        public static DeviceState fromPinState(PinState pinState) {
            if (pinState == PinState.HIGH)
                return OFF;
            return ON;
        }
    }
}
