package com.ndubkov.model;


import com.google.gson.annotations.Expose;

/**
 * Created by Nickolay on 17.11.2016.
 */

public class Response<T> {

    ResponseStatus responseStatus;
    String responseMessage;
    T data;

    public Response(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Response(ResponseStatus responseStatus, String responseMessage) {
        this.responseStatus = responseStatus;
        this.responseMessage = responseMessage;
    }

    public Response(ResponseStatus responseStatus, T data) {
        this.responseStatus = responseStatus;
        this.data = data;
    }

    public Response(ResponseStatus responseStatus, String responseMessage, T data) {
        this.responseStatus = responseStatus;
        this.responseMessage = responseMessage;
        this.data = data;
    }

    public enum ResponseStatus {
        SUCCESS, FAILED
    }
}
