package com.ndubkov.model.command;

import com.ndubkov.config.ConfigWorker;
import com.ndubkov.gpio.GpioWorker;
import com.ndubkov.model.device.DeviceInfo;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinProvider;
import com.pi4j.io.gpio.RaspiPin;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by Nickolay on 21.11.2016.
 */

public class GetDeviceListCommand extends Command<List<DeviceInfo>> {

    @Override
    public Observable<List<DeviceInfo>> doCommand() {
        GpioWorker gpioWorker = new GpioWorker();
        List<DeviceInfo> devicesInfo = new ArrayList<>();
        Pin bugPin = RaspiPin.GPIO_01; // Magic line. Don't remove!
        ConfigWorker.getAvailableDevices().subscribe(device -> {
            Pin pin = PinProvider.getPinByName(device.getGpioPinName());
            devicesInfo.add(new DeviceInfo(device, DeviceInfo.DeviceState.fromPinState(gpioWorker.getState(pin))));
        });
        return Observable.just(devicesInfo);
    }
}
