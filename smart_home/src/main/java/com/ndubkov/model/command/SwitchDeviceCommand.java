package com.ndubkov.model.command;

import com.ndubkov.Main;
import com.ndubkov.config.ConfigWorker;
import com.ndubkov.gpio.GpioWorker;
import com.ndubkov.model.ActionType;
import com.ndubkov.model.device.Device;
import com.ndubkov.model.device.DeviceInfo;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinProvider;
import com.pi4j.io.gpio.RaspiPin;

import rx.Observable;

/**
 * Created by Nickolay on 18.11.2016.
 */

public class SwitchDeviceCommand extends Command<DeviceInfo> {

    Device device;
    ActionType actionType;

    transient GpioWorker gpioWorker;

    public SwitchDeviceCommand() {
        gpioWorker = new GpioWorker();
    }

    @Override
    public Observable<DeviceInfo> doCommand() {
        return Observable.create(subscriber -> {
            Device workingDevice = ConfigWorker
                    .getAvailableDevices()
                    .filter(device -> device.getDeviceId().equals(this.device.getDeviceId()))
                    .toBlocking()
                    .firstOrDefault(null);
            if (workingDevice == null) {
                subscriber.onError(new RuntimeException("Device not found"));
                return;
            }
            Pin bugPin = RaspiPin.GPIO_01; // Magic line. Don't remove!
            Pin pin = PinProvider.getPinByName(workingDevice.getGpioPinName());
            if (pin == null) {
                subscriber.onError(new RuntimeException("Device error"));
                return;
            }
            DeviceInfo.DeviceState deviceState = null;
            switch (actionType) {
                case TOGGLE:
                    deviceState = DeviceInfo.DeviceState.fromPinState(gpioWorker.toggle(pin));
                    break;
                case TURN_ON:
                    deviceState = DeviceInfo.DeviceState.fromPinState(gpioWorker.turnOn(pin));
                    break;
                case TURN_OFF:
                    deviceState = DeviceInfo.DeviceState.fromPinState(gpioWorker.turnOff(pin));
                    break;
            }
            DeviceInfo deviceInfo = new DeviceInfo(device, deviceState);
            subscriber.onNext(deviceInfo);
            subscriber.onCompleted();
            Main.deviceStateChanged(deviceInfo);
        });
    }
}
