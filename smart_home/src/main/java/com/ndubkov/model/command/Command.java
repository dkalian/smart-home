package com.ndubkov.model.command;

import rx.Observable;

/**
 * Created by Nickolay on 18.11.2016.
 */

public abstract class Command<T> {

    public abstract Observable<T> doCommand();
}
