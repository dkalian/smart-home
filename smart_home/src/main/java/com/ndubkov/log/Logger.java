package com.ndubkov.log;

import com.google.gson.GsonBuilder;

/**
 * Created by Nickolay on 18.11.2016.
 */

public class Logger {

    public static void log(String message) {
        System.out.println(message);
    }

    public static void log(String message, Object objectToPrint) {
        System.out.println(message + ":\n" + new GsonBuilder().setPrettyPrinting().create().toJson(objectToPrint));
    }
}
