package com.ndubkov.myhome.api.core;


import com.google.gson.reflect.TypeToken;
import com.ndubkov.myhome.api.core.model.command.ActionType;
import com.ndubkov.myhome.api.core.model.command.GetDeviceListCommand;
import com.ndubkov.myhome.api.core.model.device.Device;
import com.ndubkov.myhome.api.core.model.device.DeviceInfo;
import com.ndubkov.myhome.api.core.model.request.Request;
import com.ndubkov.myhome.api.core.model.Response;
import com.ndubkov.myhome.api.core.model.command.SwitchDeviceCommand;

import java.lang.reflect.Type;
import java.util.List;

import rx.Observable;

/**
 * Created by Nickolai on 03.10.2016.
 */

public class DevicesManager {

    SmartHomeConnector.Connection mConnection;

    public DevicesManager(SmartHomeConnector.Connection connection) {
        this.mConnection = connection;
    }

    public Observable<Response<DeviceInfo>> switchDevice(Device device, ActionType actionType) {
        SwitchDeviceCommand switchDeviceCommand = new SwitchDeviceCommand(device, actionType);
        Request request = new Request(switchDeviceCommand, SwitchDeviceCommand.class);
        Type responseType = new TypeToken<Response<DeviceInfo>>() {
        }.getType();
        return mConnection.sendCommand(request, responseType);
    }

    public Observable<Response<List<DeviceInfo>>> getAvailableDevices() {
        GetDeviceListCommand deviceListCommand = new GetDeviceListCommand();
        Request request = new Request(deviceListCommand, GetDeviceListCommand.class);
        Type responseType = new TypeToken<Response<List<DeviceInfo>>>() {
        }.getType();
        return mConnection.sendCommand(request, responseType);
    }

    public void disconnect() {
        mConnection.close();
    }
}
