package com.ndubkov.myhome.api.core.model.request;

import com.ndubkov.myhome.api.core.model.command.Command;

/**
 * Created by Nickolay on 18.11.2016.
 */

public class Request {

    String commandType;
    Command command;

    public Request(Command command, Class commandClass) {
        this.commandType = commandClass.getSimpleName();
        this.command = command;
    }

    public String getCommandType() {
        return commandType;
    }
}
