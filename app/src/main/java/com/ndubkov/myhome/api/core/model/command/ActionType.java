package com.ndubkov.myhome.api.core.model.command;

/**
 * Created by Nickolai on 03.10.2016.
 */

public enum ActionType {
    TOGGLE, TURN_ON, TURN_OFF
}