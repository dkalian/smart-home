package com.ndubkov.myhome.api.core;

import android.util.Log;

import com.google.gson.Gson;
import com.ndubkov.myhome.api.core.model.device.DeviceInfo;
import com.ndubkov.myhome.api.core.model.request.Request;
import com.ndubkov.myhome.test.Test;
import com.ndubkov.myhome.api.core.model.Response;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.locks.ReentrantLock;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Nickolai on 03.10.2016.
 */

public class SmartHomeConnector {

    private static final String TAG = "SmartHomeConnector";
    private static final String DEFAULT_IP = "RASPBERRYPI";
    private static final int DEFAULT_PORT = 4444;

    /**
     * Connect to smart home by default connection
     *
     * @return connection
     * @see Connection
     */
    public Observable<Connection> connect() {
        if (!Test.isEmulator())
            return connect(DEFAULT_IP);
        return connect("192.168.137.80");
    }

    /**
     * Connect to smart home by IP address ot host name.
     * Port will be {@link SmartHomeConnector#DEFAULT_PORT}
     *
     * @param address IP address or hostname
     * @return connection
     * @see Connection
     */
    public Observable<Connection> connect(String address) {
        return connect(address, DEFAULT_PORT);
    }

    /**
     * Connect to smart home by IP address or host name and port
     *
     * @param address IP address or hostname
     * @param port    port
     * @return connection
     * @see Connection
     */
    public Observable<Connection> connect(String address, int port) {
        return Observable.create(subscriber -> {
            try {
                Socket serverSocket = new Socket(address, port);
                subscriber.onNext(new Connection(serverSocket));
            } catch (IOException e) {
                new RuntimeException(e);
                subscriber.onError(e);
            }
            subscriber.onCompleted();
        });
    }

    public class Connection {
        Socket serverSocket;

        Connection(Socket serverSocket) throws IOException {
            this.serverSocket = serverSocket;
        }

        public void close() {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        <T> Observable<Response<T>> sendCommand(final Request request, final Type responseType) {
            return Observable.create(subscriber -> {
                try {
                    new PrintWriter(serverSocket.getOutputStream(), true).println(new Gson().toJson(request));
                    getServerMessage().subscribe(responseText -> {
                        Response<T> response = new Gson().fromJson(responseText, responseType);
                        if (response.getResponseStatus() == Response.ResponseStatus.FAILED) {
                            if (response.getResponseMessage() != null)
                                subscriber.onError(new RuntimeException(response.getResponseMessage()));
                            subscriber.onError(new RuntimeException("Something went wrong"));
                        }
                        subscriber.onNext(response);
                        subscriber.onCompleted();
                    });
                } catch (IOException e) {
                    subscriber.onError(e);
                }
            });
        }

        Observable<String> getServerMessage() {
            return Observable.create(subscriber -> {
                try {
                    BufferedReader fromServer = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
                    String responseText = fromServer.readLine();
                    if (responseText == null)
                        subscriber.onError(new RuntimeException("Server closed connection"));
                    subscriber.onNext(responseText);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
