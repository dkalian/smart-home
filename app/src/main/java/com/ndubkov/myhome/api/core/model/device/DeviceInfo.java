package com.ndubkov.myhome.api.core.model.device;

import com.ndubkov.myhome.api.core.model.device.Device;

/**
 * Created by Nickolay on 17.11.2016.
 */

public class DeviceInfo {
    Device device;
    DeviceState deviceState;

    public DeviceInfo(String deviceId, DeviceState deviceState) {
        this.device = new Device(deviceId);
        this.deviceState = deviceState;
    }

    public DeviceInfo(Device device, DeviceState deviceState) {
        this.device = device;
        this.deviceState = deviceState;
    }

    public enum DeviceState {
        ON, OFF;
    }

    public Device getDevice() {
        return device;
    }

    public DeviceState getDeviceState() {
        return deviceState;
    }
}
