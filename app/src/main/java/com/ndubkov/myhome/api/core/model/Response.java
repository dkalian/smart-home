package com.ndubkov.myhome.api.core.model;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Nickolay on 17.11.2016.
 */

public class Response<T> {

    ResponseStatus responseStatus;
    String responseMessage;
    T data;

    public Response(@NonNull ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Response(@NonNull ResponseStatus responseStatus, @Nullable String responseMessage) {
        this.responseStatus = responseStatus;
        this.responseMessage = responseMessage;
    }

    public Response(@NonNull ResponseStatus responseStatus, @Nullable String responseMessage, T data) {
        this.responseStatus = responseStatus;
        this.responseMessage = responseMessage;
        this.data = data;
    }

    public enum ResponseStatus {
        SUCCESS, FAILED
    }

    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public T getData() {
        return data;
    }
}
