package com.ndubkov.myhome.api.core.model.device;

import com.google.gson.annotations.Expose;

/**
 * Created by Nickolay on 17.11.2016.
 */

public class Device {

    String deviceId;
    String deviceTitle;
    String deviceCategory;

    public Device(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getDeviceTitle() {
        return deviceTitle;
    }

    public String getDeviceCategory() {
        return deviceCategory;
    }
}
