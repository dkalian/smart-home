package com.ndubkov.myhome.api.core.model.command;

import com.ndubkov.myhome.api.core.model.device.Device;

/**
 * Created by Nickolay on 18.11.2016.
 */

public class SwitchDeviceCommand extends Command{

    Device device;
    ActionType actionType;

    public SwitchDeviceCommand(Device device, ActionType actionType) {
        this.device = device;
        this.actionType = actionType;
    }
}
