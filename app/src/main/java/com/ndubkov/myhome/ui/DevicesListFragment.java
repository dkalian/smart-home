package com.ndubkov.myhome.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.GsonBuilder;
import com.ndubkov.myhome.R;
import com.ndubkov.myhome.api.core.DevicesManager;
import com.ndubkov.myhome.api.core.SmartHomeConnector;
import com.ndubkov.myhome.api.core.model.command.ActionType;
import com.ndubkov.myhome.api.core.model.device.Device;
import com.ndubkov.myhome.ui.adapter.DeviceListAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DevicesListFragment extends Fragment {

    private static final String TAG = "DevicesListFragment";

    DeviceListAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new DeviceListAdapter(getContext());
        mAdapter.setDeviceChangeListener(mDeviceChangeListener);
        EventBus.getDefault().register(this);
    }

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    View mRootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_devices_list, container, false);
        return mRootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, mRootView);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    SmartHomeConnector.Connection mConnection;

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(SmartHomeConnector.Connection connection) {
        Log.i(TAG, "onEvent: connection");
        mConnection = connection;
        Snackbar.make(mRootView, "Connected", Snackbar.LENGTH_LONG).show();
        new DevicesManager(mConnection).getAvailableDevices()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(deviceListResponse -> {
                    Log.d(TAG, "onEvent: get device list");
                    mAdapter.updateDataSet(deviceListResponse.getData());
                    mAdapter.notifyDataSetChanged();
                }, throwable -> {
                    Log.e(TAG, "onEvent: get device list error");
                    throwable.printStackTrace();
                    Snackbar.make(mRootView, throwable.getMessage(), Snackbar.LENGTH_LONG).show();
                });
    }

    DeviceListAdapter.DeviceChangeListener mDeviceChangeListener = (device, actionType, pos) ->
            new DevicesManager(mConnection)
                    .switchDevice(device, actionType)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(deviceInfoResponse -> {
                        Log.d(TAG, "device change listener: success");
                        mAdapter.update(pos, deviceInfoResponse.getData());
                        mAdapter.notifyItemChanged(pos);
                    }, throwable -> {
                        Log.e(TAG, "device change listener: error");
                        throwable.printStackTrace();
                        Snackbar.make(mRootView, throwable.getMessage(), Snackbar.LENGTH_LONG).show();
                    });
}
