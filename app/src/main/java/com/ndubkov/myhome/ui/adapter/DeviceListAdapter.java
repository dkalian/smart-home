package com.ndubkov.myhome.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Checkable;
import android.widget.Switch;
import android.widget.TextView;

import com.ndubkov.myhome.R;
import com.ndubkov.myhome.api.core.model.Response;
import com.ndubkov.myhome.api.core.model.command.ActionType;
import com.ndubkov.myhome.api.core.model.device.Device;
import com.ndubkov.myhome.api.core.model.device.DeviceInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nickolay on 18.11.2016.
 */

public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.DeviceViewHolder> {

    List<DeviceInfo> mDataSet = new ArrayList<>();
    Context mContext;

    public DeviceListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public DeviceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new DeviceViewHolder(inflater.inflate(R.layout.item_device, parent, false));
    }

    DeviceChangeListener mDeviceChangeListener;

    @Override
    public void onBindViewHolder(DeviceViewHolder holder, int position) {
        DeviceInfo deviceInfo = mDataSet.get(holder.getAdapterPosition());
        if (deviceInfo == null)
            return;
        holder.title.setText(deviceInfo.getDevice().getDeviceTitle());
        holder.swState.setEnabled(true);
        holder.swState.setChecked(deviceInfo.getDeviceState() == DeviceInfo.DeviceState.ON);
        holder.swState.setOnClickListener(v -> {
            if (mDeviceChangeListener != null) {
                holder.swState.setChecked(!holder.swState.isChecked());
                holder.swState.setEnabled(false);
                mDeviceChangeListener.onChanged(deviceInfo.getDevice(),
                        ((Checkable) v).isChecked() ? ActionType.TURN_OFF : ActionType.TURN_ON,
                        holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public void updateDataSet(List<DeviceInfo> deviceList) {
        mDataSet = deviceList;
    }

    public void setDeviceChangeListener(DeviceChangeListener deviceChangeListener) {
        this.mDeviceChangeListener = deviceChangeListener;
    }

    public void update(int pos, DeviceInfo deviceInfo) {
        mDataSet.set(pos, deviceInfo);
    }

    public interface DeviceChangeListener {
        void onChanged(Device device, ActionType actionType, int pos);
    }

    public class DeviceViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_title)
        TextView title;
        @BindView(R.id.sw_state)
        Switch swState;

        public DeviceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
