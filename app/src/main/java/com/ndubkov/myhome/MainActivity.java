package com.ndubkov.myhome;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.ndubkov.myhome.api.core.SmartHomeConnector;
import com.ndubkov.myhome.ui.DevicesListFragment;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    SmartHomeConnector.Connection mConnection;
    @BindView(R.id.container)
    View mContainerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new DevicesListFragment())
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        connect();
    }

    void connect() {
        Observable<SmartHomeConnector.Connection> connectionObservable = new SmartHomeConnector().connect();
        connectionObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onConnected, throwable -> {
                            throwable.printStackTrace();
                            Snackbar.make(mContainerView, "Сервер недоступен", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("ПОВТОРИТЬ", v -> connect())
                                    .show();
                        }
                );
    }

    void onConnected(SmartHomeConnector.Connection connection) {
        mConnection = connection;
        EventBus.getDefault().postSticky(connection);
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mConnection != null) {
            mConnection.close();
            EventBus.getDefault().removeStickyEvent(mConnection);
        }
    }
}
